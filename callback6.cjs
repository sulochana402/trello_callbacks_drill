/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
const callback1ThanosBoards = require("./callback1.cjs");
const callback2ThanosLists = require("./callback2.cjs");
const callbackAllListCards = require("./callback3.cjs");

function callback6(thanosId) {
  setTimeout(() => {
    callback1ThanosBoards(thanosId, (err, board) => {
      if (err) {
        console.error(err);
      } else {
        console.log("Thanos Board ID:", board);
        callback2ThanosLists(thanosId, (err, lists) => {
          if (err) {
            console.error(err);
          } else {
            console.log("Lists for Thanos Board:", lists);
            for (let list of lists) {
              callbackAllListCards(list.id, (err, cards) => {
                if (err) {
                  console.error(err);
                } else {
                  console.log("All list cards:",list.id, cards);
                }
              });
            }
          }
        });
      }
    });
  }, 2 * 1000);
}
module.exports = callback6;
