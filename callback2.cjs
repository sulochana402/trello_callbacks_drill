/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

// const boards = require("./boards.json");
const lists = require("./lists.json");

function callback2(boardID, callback) {
  if (typeof boardID === "string") {
    setTimeout(() => {
      // Check if the boardID exists in the lists data
      if (boardID in lists) {
        callback(null, lists[boardID]);
      } else {
        callback("BoardId is not found");
      }
    }, 2 * 1000);
  } else {
    console.error("BoardId is not a string");
  }
}
module.exports = callback2;
