const callback2 = require("../callback2.cjs");

const boardID = "mcu453ed";
callback2(boardID,(err,data)=>{
    if(err){
        console.error(err);
    } else{
        console.log("Lists belonging to Board:",data);
    }
});