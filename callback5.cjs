/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const callback1ThanosBoards = require("./callback1.cjs");
const callback2ThanosLists = require("./callback2.cjs");
const callback3MindListCards = require("./callback3.cjs");

function callback5(thanosId) {
  setTimeout(() => {
    callback1ThanosBoards(thanosId, (err, board) => {
      if (err) {
        console.error(err);
      } else {
        console.log("Thanos Board ID:", board);
        callback2ThanosLists(thanosId, (err, lists) => {
          if (err) {
            console.error(err);
          } else {
            console.log("Lists for Thanos Board:", lists);
            const mindList = lists.find(list => list.name === "Mind");
            const spaceList = lists.find(list => list.name === "Space");
            callback3MindListCards(mindList.id, (err, cards) => {
              if (err) {
                console.error(err);
              } else {
                console.log("cards for Mind list:", cards);
                callback3MindListCards(spaceList.id, (err, cards) => {
                  if (err) {
                    console.error(err);
                  } else {
                    console.log("cards for Space list:", cards);
                  }
                });
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}
module.exports = callback5;
