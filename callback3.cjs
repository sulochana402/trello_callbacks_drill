/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const cards = require("./cards.json");

function callback3(listID, callback) {
  if (typeof listID === "string") {
    setTimeout(() => {
      if (listID in cards) {
        callback(null, cards[listID]);
      } else {
        callback("ListId is not found");
      }
    }, 2 * 1000);
  } else {
    console.error("ListId is not a string");
  }
}
module.exports = callback3;
