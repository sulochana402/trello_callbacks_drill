/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const callback1ThanosBoards = require("./callback1.cjs");
const callback2ThanosLists = require("./callback2.cjs");
const callback3MindListCards = require("./callback3.cjs");

function callback4(thanosId,listName) {
  setTimeout(() => {
    callback1ThanosBoards(thanosId, (err, board) => {
      if (err) {
        console.error(err);
      } else {
        console.log("Thanos Board ID:", board);
        callback2ThanosLists(thanosId, (err, listsId) => {
          if (err) {
            console.error(err);
          } else {
            console.log("Lists for Thanos Board:", listsId);
            const mindListCards = listsId.find((list)=>{
              if(list.name === listName){
                return list;
              }
            });
            callback3MindListCards(mindListCards.id, (err, cards) => {
              if (err) {
                console.error(err);
              } else {
                console.log("Cards for Mind List:", cards);
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}
module.exports = callback4;
