/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const boards = require("./boards.json");

function callback1(boardID, callback) {
  if (typeof boardID === "string") {
    setTimeout(() => {
      const boardInfo = boards.find((board) => {
        return board.id === boardID;
      });
      if (boardInfo) {
        callback(null, boardInfo);
      } else {
        callback("Board not found");
      }
    }, 2 * 1000);
  } else {
    console.error("Board is not a string");
  }
}
module.exports = callback1;
