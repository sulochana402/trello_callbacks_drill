const callback1 = require("../callback1.cjs");

const boardID = "mcu453ed";

callback1(boardID,(err,data)=>{
    if(err){
        console.error("Error:",err);
    } else{
        console.log("Board Information:", data);
    }
});